# frozen_string_literal: true

class UserMailer < ApplicationMailer
  def welcome_email(user)
    @user = user
    mail to: user.email, subject: 'Welcome email'
  end

  def password_reset(user)
    @user = user
    mail to: user.email, subject: 'Password Reset'
  end
end
