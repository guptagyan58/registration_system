# frozen_string_literal: true

class UsersController < ApplicationController
  before_action :find_user, only: %i[edit update show destroy]

  def new
    @user = User.new
  end

  def edit; end

  def show; end

  def create
    @user = User.new(user_params)
    @user.username = @user.email.split('@').first
    if @user.save
      cookies.permanent[:auth_token] = @user.auth_token
      redirect_to '/', notice: 'Sign up successfully.'
    else
      redirect_to '/signup', alert: @user.errors.full_messages.join(', ')
    end
  end

  def update
    if @user.update(user_params)
      redirect_to '/', notice: 'User updated successfully.'
    else
      redirect_to edit_user_path(@user), alert: @user.errors.full_messages.join(', ')
    end
  end

  def destroy
    if @user.destroy
      session[:user_id] = nil
      redirect_to '/login', notice: 'Successfully deleted user account.'
    else
      flash[:alert] = 'Error while deleting user account.'
    end
  end

  private

  def find_user
    @user = User.find(params[:id])
  end

  def user_params
    params.require(:user).permit(:username, :email, :password, :password_confirmation)
  end
end
