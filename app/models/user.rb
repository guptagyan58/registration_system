# frozen_string_literal: true

class User < ApplicationRecord
  has_secure_password
  validates :email, presence: true, uniqueness: true, email: true
  validates :password, presence: true, length: { minimum: 8 }, on: :user_create_action
  validates :username, presence: true, length: { minimum: 5 }, on: :update
  before_create { generate_token(:auth_token) }
  after_create :send_welcome_email

  def generate_token(column)
    begin
      self[column] = SecureRandom.urlsafe_base64
    end while User.exists?(column => self[column])
  end

  def send_password_reset
    generate_token(:password_reset_token)
    self.password_reset_sent_at = Time.zone.now
    save!
    UserMailer.password_reset(self).deliver
  end

  private

  def send_welcome_email
    UserMailer.welcome_email(self).deliver
  end
end
