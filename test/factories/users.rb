# frozen_string_literal: true

FactoryBot.define do
  factory(:user) do
    sequence(:email) { |n| "test_#{n}@example.com" }
    sequence(:username) { |n| "test_#{n}" }
    password { '12345678' }
    password_confirmation { '12345678' }
  end
end
