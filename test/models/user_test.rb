# frozen_string_literal: true

require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test 'valid user' do
    user = User.new(username: 'john', email: 'john@example.com', password:
      '12345678', password_confirmation: '12345678')
    assert user.valid?
  end

  test 'invalid without proper email' do
    user = User.new(email: 'john')
    refute user.valid?, 'user is valid without a proper email'
    assert_equal user.errors[:email], ['is not an email']
  end
end
