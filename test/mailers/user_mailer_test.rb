# frozen_string_literal: true

require 'test_helper'

class UserMailerTest < ActionMailer::TestCase
  def setup
    @user = create(:user)
  end

  test 'welcome user' do
    email = UserMailer.welcome_email(@user)
    assert_equal [@user.email], email.to
  end
end
