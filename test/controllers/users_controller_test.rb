# frozen_string_literal: true

require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @user = create(:user)
  end

  test 'should create user' do
    assert_difference('User.count') do
      post users_url, params: { user: { email: 'testing@mailinator.com', password:
        '12345678', password_confirmation: '12345678' } }
    end
    assert_response :redirect
    assert_equal 'Sign up successfully.', flash[:notice]
  end

  test 'should not create user with invalid email' do
    post users_url, params: { user: { email: 'testing', password:
      '12345678', password_confirmation: '12345678' } }
    assert_response :redirect
    assert_equal 'Email is not an email', flash[:alert]
  end

  test 'should update user' do
    patch user_url(@user), params: { user: { username: 'testing' } }
    assert_response :redirect
    assert_equal 'User updated successfully.', flash[:notice]
  end

  test 'should not update user with invalid username' do
    patch user_url(@user), params: { user: { username: 'test' } }
    assert_response :redirect
    assert_equal 'Username is too short (minimum is 5 characters)', flash[:alert]
  end

  test 'should destroy user' do
    assert_difference('User.count', -1) do
      delete user_url(@user)
    end
    assert_response :redirect
    assert_equal 'Successfully deleted user account.', flash[:notice]
  end
end
