# frozen_string_literal: true

require 'test_helper'

class SessionsControllerTest < ActionDispatch::IntegrationTest
  test 'should authenticate_login' do
    @user = create(:user)
    post login_url, params: { email: @user.email, password: '12345678' }
    assert_equal 'Logged in!', flash[:notice]
    assert_response :redirect
  end

  test 'user should be authenticable' do
    user = create(:user, password: 'password', password_confirmation: 'password')
    assert_not(user.authenticate('qwerty'))
    assert(user.authenticate('password'))
  end

  test 'should logout' do
    @user = create(:user)
    get logout_url(@user)
    assert_equal 'Logged out!', flash[:notice]
    assert_response :redirect
  end
end
