# RegistrationSystem

## General information
**RegistrationSystem** is a web application which allows user registration and login functionality. Also have forget password functionality for user to rest password.
After login user can update their information on profile page.

### Project resources

If you are reading this, you are most likely a member of the awesome team behind registartion & login system, so you should already have access to following resources:

- https://gitlab.com/guptagyan58/registration_system - this is project Gitlab repository.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

To run the project locally you should have following software installed on your machine:

- Git
- Ruby 2.6.1
- Ruby-bundler
- PostgreSQL 11.2


#### Step 1: In your terminal, navigate to your projects directory and clone this repository

- cd YOUR-RAILS-PROJECTS
- git clone https://gitlab.com/guptagyan58/registration_system.git


#### Step 2: Now navigate to the created directory `registraion_system` and run `bundle install`

- cd registraion_system
- gem install bundler
- bundle install

#### Step 3: Set up databases

- rails db:create db:migrate

Above command will create your development and test databases.

## Running app

Run Rails server:

- rails s

## Running tests

- rake test
